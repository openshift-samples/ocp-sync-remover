# OpenShift-Sync Remover

Transform your BuildConfig pipeline into a simple Jenkins Job

## Adjust the job-template.xml to your Jenkins version

the job template may very, so before trying you may check an example in your Jenkins server

### Retrieve jenkins config from jenkins

**Variables**

* {url}: for example jenkins-automation.192.168.42.190.nip.io/job/automation
* {job_path}: for example job/automation-jenkins-pipeline-demo
* {user}: for example admin-admin-edit-view
* {token}: for example 11ea33c0642018b67ac6d8f99c79168026


    curl -k -X  GET https://{url}/{job_path}/config.xml -u {user}:{token} -o job.xml

## Jenkins Job Parameters

| **Parameter** | **Description**|
| --- | --- |
| USER | jenkins user |
| TOKEN | jenkins user's API token |  
| JENKINS_URL | Jenkins URL to create the new Job |  
| BC_NAME | Name of BuildConfig you want to migrate |  
| NAMESPACE | Namespace where the BC is |  
